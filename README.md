# bx-cli

This script is inspired by `bem-cli`. Install this globally and you'll have access to the `bx` command anywhere on your system.

If you hate to run local bem in this way `./node_modules/.bin/bx` this tool is for you!

```shell
npm install -g bx-cli
```
